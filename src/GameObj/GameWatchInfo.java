
package GameObj;

import Objects.Time;
import Objects.Web;
import java.net.UnknownHostException;

public class GameWatchInfo {
    private String cdn, quality, url, date;

    public GameWatchInfo(String cdn, String quality, String date, String mediaID) {
        this.cdn = cdn;
        this.quality = quality;
        url = Web.getContent("http://mf.svc.nhl.com/m3u8/" + date + "/" + mediaID + ".txt");
    }

    public GameWatchInfo() {
        
    }

    /**
     * @return the cdn
     */
    public String getCdn() {
        return cdn;
    }

    /**
     * @param cdn the cdn to set
     */
    public void setCdn(String cdn) {
        this.cdn = cdn;
    }

    /**
     * @return the quality
     */
    public String getQuality() {
        return quality;
    }

    /**
     * @param quality the quality to set
     */
    public void setQuality(String quality) {
        this.quality = quality;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    public void setUrl(String date, String mediaID) {
        url = Web.getContent("http://mf.svc.nhl.com/m3u8/" + date + "/" + mediaID);
        
        if (!Time.isToday(date)) {
            if (!url.contains("vod")) {
                String urlvod = url.replace("l3c", "akc").replace("ls04", "ps01").replace("live", "vod");
                try{
                if (Web.testM3U8(urlvod)) {
                    url = urlvod;
                }
                } catch(UnknownHostException e) {}
            }
        }
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
